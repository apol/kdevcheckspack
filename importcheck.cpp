/*  This file is part of KDevelop
    Copyright 2011 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include <interfaces/ilanguagecheck.h>
#include <interfaces/iassistant.h>
#include <KUrl>
#include <language/duchain/duchainlock.h>
#include <language/duchain/duchain.h>
#include <language/duchain/use.h>
#include <language/duchain/declaration.h>
#include <language/codegen/documentchangeset.h>
#include <KLocalizedString>
#include "kdevcheckspackplugin.h"

using namespace KDevelop;


class ImportCheck : public ILanguageCheck
{
    virtual QString name() const { return i18n("Unused Import"); }
    virtual void runCheck(const KDevelop::CheckData& data);
};

REGISTER_CHECK(ImportCheck)

struct IncludeProblemItem
{
    IncludeProblemItem(const KUrl& url, const KUrl& sourceUrl, KDevelop::CursorInRevision cursor)
        : url(url), sourceUrl(sourceUrl), cursor(cursor) {}
    KUrl url;
    KUrl sourceUrl;
    KDevelop::CursorInRevision cursor;
};

namespace {

QList<const Use*> usesForCtx(const DUContext* ctx)
{
    QList<const Use*> ret;
    for(int i=0; i<ctx->usesCount(); ++i) {
        ret += &ctx->uses()[i];
    }

    foreach(const DUContext* child, ctx->childContexts()) {
        ret += usesForCtx(child);
    }

    return ret;
}

QList<IncludeProblemItem> includes(KDevelop::TopDUContext* top)
{
    DUChainReadLocker lock(DUChain::lock());
    QVector<DUContext::Import> imported=top->importedParentContexts();
    if(imported.isEmpty())
        return QList<IncludeProblemItem>();

    QVector<Declaration*> decls;

    QList<const Use*> uses=usesForCtx(top);
    Q_FOREACH(const Use* use, uses) {
        Declaration* decl=use->usedDeclaration(top);

        if(decl && decl->topContext()!=top) //let's just add the ones that are not declared in the same file
            decls.append(decl);
    }

    QList<IncludeProblemItem> ret;
    
    foreach(const DUContext::Import& import, imported) {
        IndexedDUContext ctx = import.indexedContext();

        Declaration* which=0;
        foreach(Declaration* d, decls) {
            if(ctx.data()->imports(d->context())) {
                which=d;   
            }
        }

        if(!which) {
            IndexedString inc=ctx.context()->url();
            ret << IncludeProblemItem(inc.toUrl(), top->url().toUrl(), import.position);
        }
    }
    return ret;
}

}

class RemoveImportAction : public KDevelop::IAssistantAction
{
    public:
        RemoveImportAction(const IncludeProblemItem& include)
            : m_include(include) {}
        virtual QString description() const { return i18n("Remove import"); }
        virtual void execute() {
            KDevelop::DocumentChangeSet changes;

            SimpleCursor endCursor(m_include.cursor.line+1, 0);
            DocumentChange change(IndexedString(m_include.sourceUrl), SimpleRange(m_include.cursor.castToSimpleCursor(), endCursor), QString(), QString());
            change.m_ignoreOldText = true;
            changes.addChange(change);

            DocumentChangeSet::ChangeResult result = changes.applyAllChanges();
            if(!result)
                qWarning() << "error: could not remove" << m_include.url << result.m_failureReason << result.m_reasonChange;
        }
    private:
        IncludeProblemItem m_include;
};

class UnneededImportAssistant : public KDevelop::IAssistant
{
    public:
        UnneededImportAssistant(const IncludeProblemItem& include)
            : m_include(include) {}
        virtual QString title() const { return i18n("Wrong import"); }
        virtual QList<IAssistantAction::Ptr> actions() const {
            return QList<IAssistantAction::Ptr>() << IAssistantAction::Ptr(new RemoveImportAction(m_include));
        }
private:
    IncludeProblemItem m_include;
};

void ImportCheck::runCheck(const KDevelop::CheckData& data)
{
    QList<IncludeProblemItem> incls = includes(data.top);
    
    Q_FOREACH(const IncludeProblemItem &include, incls) {
        DUChainWriteLocker lock(DUChain::lock());
        ProblemPointer p(new Problem);
        p->setRange(RangeInRevision(include.cursor, include.cursor));
        p->setFinalLocation(DocumentRange(IndexedString(data.url), p->range().castToSimpleRange()));
        p->setDescription(i18n("Unused import '%1'", include.url.prettyUrl()));
        p->setSeverity(KDevelop::ProblemData::Hint);
        p->setSource(KDevelop::ProblemData::SemanticAnalysis); //?
        p->setSolutionAssistant(KSharedPtr<IAssistant>(new UnneededImportAssistant(include)));
        data.top->addProblem(p);
        
        qDebug() << "added problem" << data.url << include.cursor;
    }
}
