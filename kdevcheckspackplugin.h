/*  This file is part of KDevelop
    Copyright 2011 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef KDEVCHECKSPACKPLUGIN_H
#define KDEVCHECKSPACKPLUGIN_H

#include <interfaces/iplugin.h>
#include <interfaces/ilanguagecheckprovider.h>
#include <QVariantList>

#define REGISTER_CHECK(className) \
namespace { ILanguageCheck* f() { return new className ; } bool _b=KDevChecksPackPlugin::registerCheck(f); }

class KDevChecksPackPlugin : public KDevelop::IPlugin, public KDevelop::ILanguageCheckProvider
{
	Q_OBJECT
    Q_INTERFACES( KDevelop::ILanguageCheckProvider )
	public:
		KDevChecksPackPlugin(QObject *parent, const QVariantList & args);
		
		virtual QList< KDevelop::ILanguageCheck* > providedChecks();
		
		typedef KDevelop::ILanguageCheck* (*checkInitializer)();
		static bool registerCheck(checkInitializer init);
		
	private:
		void initializeChecks();
		QList< KDevelop::ILanguageCheck* > m_checks;
};

#endif
