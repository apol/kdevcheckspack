/*  This file is part of KDevelop
    Copyright 2011 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "kdevcheckspackplugin.h"
#include <kpluginfactory.h>
#include <KAboutData>

K_PLUGIN_FACTORY(KDevChecksPackFactory, registerPlugin<KDevChecksPackPlugin>(); )
K_EXPORT_PLUGIN(KDevChecksPackFactory(
    KAboutData("kdevcheckspack","kdevcheckspack",
               ki18n("KDevelop Checks Pack"), "0.1", ki18n("Provides different checks to analyze your code"), KAboutData::License_GPL)))

using namespace KDevelop;

class InitializersSingleton
{
public:
    static InitializersSingleton* self()
    {
        if(!m_self) m_self=new InitializersSingleton;
        return m_self;
    }
    
    QList<KDevChecksPackPlugin::checkInitializer> m_initializers;
    static InitializersSingleton* m_self;
};
InitializersSingleton* InitializersSingleton::m_self=0;

KDevChecksPackPlugin::KDevChecksPackPlugin(QObject* parent, const QVariantList&)
    : KDevelop::IPlugin(KDevChecksPackFactory::componentData(), parent)
{
    KDEV_USE_EXTENSION_INTERFACE( KDevelop::ILanguageCheckProvider )
}

QList< ILanguageCheck* > KDevChecksPackPlugin::providedChecks()
{
	if(m_checks.isEmpty())
		initializeChecks();
	
	return m_checks;
}

void KDevChecksPackPlugin::initializeChecks()
{
	foreach(checkInitializer init_func, InitializersSingleton::self()->m_initializers)
		m_checks += init_func();
}

bool KDevChecksPackPlugin::registerCheck(checkInitializer init)
{
	InitializersSingleton::self()->m_initializers += init;
	return true;
}
