[Desktop Entry]
Type=Service
Name=KDevChecksPack
GenericName=KDevelop Checks Pack
Comment=Helps you define static checks against your code.
ServiceTypes=KDevelop/Plugin
X-KDE-Library=kdevcheckspackplugin
X-KDE-PluginInfo-Name=kdevcheckspack
X-KDE-PluginInfo-Author=Aleix Pol
X-KDE-PluginInfo-Email=aleixpol@kde.org
X-KDE-PluginInfo-License=GPL
X-KDevelop-Version=@KDEV_PLUGIN_VERSION@
X-KDevelop-Category=Global
X-KDevelop-Mode=NoGUI
X-KDevelop-LoadMode=AlwaysOn
X-KDevelop-Interfaces=org.kdevelop.ILanguageCheckProvider
